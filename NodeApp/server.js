'use strict';
var http = require('http');
var port = process.env.PORT || 1337;
var hostname = '127.0.0.1';

http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Node app at ' + hostname + 'is running at port: ' + port  + '\n');
}).listen(port);
